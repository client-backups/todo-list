var sampleTasks = [
  "Show the number of tasks",
  "Mark tasks as complete",
  "Implement task deletion with confirmation"
];

window.viewModel = {};

window.viewModel.taskList = ko.observableArray([]);

sampleTasks.forEach(function(task) {
  viewModel.taskList.push({
    text: ko.observable(task)
  });
});

window.viewModel.numberOfTasks = ko.computed(function() {
  // TODO
  return 0;
});

window.viewModel.newTask = function() {
  window.viewModel.taskList.push({
    text: ko.observable("")
  });
};

ko.applyBindings(viewModel);
