Concluir a implementação de uma lista de tarefas usando Knockout.js.

Referência: http://knockoutjs.com/


Requisitos:

* Mostrar o número total de tarefas, tarefas por concluir e tarefas concluídas
  em cada momento.

* Implementar a marcação de tarefas como completas.

  Uma tarefa está completa sempre que a checkbox respectiva está marcada.
  A caixa de texto respectiva deve ficar não-editável neste caso.

* Implementar a remoção de tarefas completas.

  Ao clicar no botão "Cleanup", as tarefas marcadas como completas devem
  ser removidas da lista.

  Antes de efectuar a remoção deve ser pedida uma confirmação ao utilizador.


Observações:

* Não é necessário persistir o estado da aplicação; as alterações são
  perdidas ao fechar a página ou fazer refresh.

* Fazer uso das funcionalidades do Knockout.js conforme seja adequado.

* Não utilizar outras frameworks ou bibliotecas de JavaScript ou CSS.
